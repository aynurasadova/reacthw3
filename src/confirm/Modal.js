import React from 'react';
import '../styles/modalStyles.scss'

export const Modal = ({header,closeIcon,actions, text, close, backgroundColor}) => {
    
    return(
    <div 
    className = "modal" 
    style={{backgroundColor}}>
        <header className = "modal-head">
            <div>{header}</div>
            <div>{closeIcon && <img className = "close-icon" onClick = {close} src = "/icons/close.png"/>}</div>
        </header>

        <main className = "modal-main">
            <p className = "modal-text">{text}</p>
            <div className = "buttons">
                {actions}
            </div>
            
        </main>
        
    </div>
    )
}