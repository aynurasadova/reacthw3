import React,{useState, useEffect} from 'react';
import { Layout } from '../../commons/Layout';
import styled from 'styled-components';
import '../../styles/totalCardStyle.scss'

export const Cart = () => {

    const [cart, setCart] = useState(() => {
        const localData = localStorage.getItem('cartItem');
        return (localData) ? JSON.parse(localData):[];
      });

      useEffect(() => {
        localStorage.setItem('cartItem', JSON.stringify(cart));
      },[cart]);

    const removeFromCart = (product_number, count) => {
        if (count > 1) {
            setCart(cart => cart.map(item => {
                if (item.product_number === product_number) {
                    return {
                        ...item,
                        count: item.count - 1
                    }
                }
                return item
            }))
        } 
    }

    const removeTotally = (product_number) => {
        setCart(cart => cart.filter(item => item.product_number !== product_number));
    }

    const addItem = (product_number, count) => {
        setCart(cart => cart.map((item) => {
            if(product_number === item.product_number) {
                return {
                    ...item,
                    count: item.count + 1
                }
            }
        })
        )
    }

    return(
        <Layout>
            <Container>
                <div className = "cart">
                    <div className = "total-detail">
                        <div 
                        style = {{
                            color: "rgba(255,255,255,.7)",
                            fontFamily: "Sanchez"}}
                        >Your Basket</div>
                        {cart.length && cart.map(({path, name, price, product_number, count }) => (
                            <div className = "single-detail" key={product_number}>
                                <div 
                                style = {{
                                    display: "flex", 
                                    alignItems: "center"}}>
                                    <img 
                                        style = {{
                                            width: "50px", 
                                            height: "62px", 
                                            borderRadius: "6px"}} src={path} alt="product image"/>
                                </div>
                                <div className = "product-name">"{name}"</div>
                                <div className = "single-price">${price}</div>
                                <button 
                                style = {{
                                        cursor: "pointer",
                                        borderStyle: "none", 
                                        outline: "none", 
                                        background: "none", 
                                        fontSize: "24px", 
                                        fontWeight: "lighter"}}
                                onClick = {() => addItem(product_number, count)}
                                >+</button>
                                <div className = "number-of-item">{count}</div>
                                <button
                                    style = {{
                                        cursor: "pointer",
                                        borderStyle: "none", 
                                        outline: "none", 
                                        background: "none", 
                                        fontSize: "24px", 
                                        fontWeight: "lighter"}}
                                    className = "remove-btn"
                                    onClick={() => removeFromCart(product_number, count)}
                                >-</button>
                                <button 
                                style = {{ 
                                        cursor: "pointer", 
                                        marginRight: "10px", 
                                        border: "1px solid grey",
                                        borderRadius: "50%", 
                                        outline: "none", 
                                        background: "none",
                                        fontWeight: "lighter"}}
                                onClick = {() => removeTotally(product_number)}
                                >X</button>
                            </div>
                        ))}
                    </div>
                    <div className = "total-price">Total price : $
                        {cart.reduce((total, { price, count }) => total + price * count, 0)}
                    </div>
                </div>
            </Container>
        </Layout>
    )
}

const Container = styled.h1`
    color: white;
`
