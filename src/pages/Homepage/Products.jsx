import React,{useEffect, useState} from 'react';
import { Layout } from '../../commons/Layout';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { SingleProduct } from '../../components';
import '../../styles/bodyStyle.scss'
import styled from 'styled-components';
import { Cart } from '../Cart';

export const Products = () => {

    const [data, setData] = useState([]);

    const getData = async () => {
        const res = await fetch('http://localhost:3001/movies');
        const dt = await res.json();
        setData(dt);
      }

    const [cart, setCart] = useState(() => {
        const localData = localStorage.getItem('cartItem');
        return (localData) ? JSON.parse(localData):[];
      });

      useEffect(() => {
        localStorage.setItem('cartItem', JSON.stringify(cart));
      },[cart]);

    const [fav, setFav] = useState(() => {
        const favData = localStorage.getItem('favorites');
        return (favData) ? JSON.parse(favData):[];
    });

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(fav))
      },[fav])
    
      const addToCart = (path, name, price, product_number, id) => {
        if (!isNaN(price)) {
          const isAvailable = cart.find((item) => product_number === item.product_number);
          if (isAvailable) {
            setCart(cart => cart.map(item => {
              
              if (item.product_number == product_number) {
                return {
                  ...item,
                  count: item.count + 1,
                }
              }
              return item
            }))
          } else {
            setCart(cart => [...cart, {
              name,
              price,
              path,
              id,
              product_number,
              count: 1
            }])
          }
        }
      }


      const addToFavorite = (name, price, path, product_number) => {
        const isAvailable = fav.find((item) => (
          product_number === item.product_number
          ))
          if(!isAvailable){
            setFav(fav => [...fav, {
              name,
              price,
              path,
              product_number,
          }])
        }
        else {return;}
      }
    
      useEffect(() => {
        getData();
      }, [])
  
      
      const removeFromFav = (product_number) => {
        setFav(fav => fav.filter(item => item.product_number !== product_number))
      }




    return (
            <Layout style = {{border: "1px solid magenta"}}>
                <div 
                className="products-list">
                    <CarouselProvider
                    className = "karusel"
                    naturalSlideWidth={100}
                    naturalSlideHeight={45}
                    totalSlides={2}
                    >
                        <Arrows>
                            <LeftArrow>{'\u003c'}</LeftArrow>
                            <RightArrow>{'\u003e'}</RightArrow>
                        </Arrows>
                        <Slider 
                        className = "slider"
                        style = {{width: "100%"}}>
                        
                        {
                            data.length ? data.map(({path, name, price, product_number}) => (
                            <div>
                                <Slide 
                                    style = {{ width: "calc(100% / 10)",overflow: "ellipses"}}>
                                    {console.log(JSON.parse(product_number))}
                                    <SingleProduct
                                    key = { product_number }
                                    fav = {fav}
                                    setFav = {setFav}
                                    path = { path }
                                    name = { name }
                                    price = { price }
                                    starIcon = "/icons/greystar.svg"
                                    addClick = {() => addToCart(path, name, price, product_number)}
                                    addFav = {() => addToFavorite(name, price, path, product_number)}
                                    removeFav = {() => removeFromFav(product_number)}
                                />
                                </Slide>
                            </div>
                            
                            )): <h1 style = {{fontFamily: "Sanchez", style: "white"}}>Loading ...</h1>
                            }
                        </Slider>
                    </CarouselProvider> 
                         
          
                </div> 
            </Layout>
    )
}

const LeftArrow = styled(ButtonBack)`
    width: 35px;
    height: 33px;
    border-radius: 5px;
    border-style: none;
    font-weight: bolder;
    color: white;
    font-family: 'Sanchez';
    background: #e14545;
`

const RightArrow = styled(ButtonNext)`
    width: 33px;
    height: 33px;
    border-radius: 5px;
    border-style: none;
    margin-left: 1px;
    font-weight: bolder;
    color: white;
    font-family: 'Sanchez';
    background: #e14545;
`

const Arrows = styled.div `
    width: auto;
    float: right;
    margin-right: 30px;
    
`

// float: right;
//     margin-right: 30px;