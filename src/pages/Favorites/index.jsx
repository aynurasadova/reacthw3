import React, {useState, useEffect} from 'react';
import { Layout } from '../../commons/Layout';
import '../../styles/totalCardStyle.scss'
import styled from 'styled-components';


export const Favorites = () => {

    const [fav, setFav] = useState(() => {
        const favData = localStorage.getItem('favorites');
        return (favData) ? JSON.parse(favData):[];
    });

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(fav))
      },[fav])

      const removeTotally = (product_number) => {
        setFav(fav => fav.filter(item => item.product_number !== product_number));
    }

    return (
        <Layout>
            <div className = "fav-cart">
                <div className = "fav-detail">
                    <div
                    style = {{
                        color: "rgba(255,255,255,.7)",
                        fontFamily: "Sanchez"}} 
                    >Favorites</div>
                    {fav.map(({path, name, price, product_number }) => (
                        <div className = "single-detail" key={product_number}>
                            <div
                            style = {{
                                display: "flex", 
                                alignItems: "center"}}
                            ><img style = {{
                                width: "50px", 
                                height: "62px",
                                borderRadius: "6px"}} src = {path}/></div>
                            <div className = "product-name">"{name}"</div>
                            <div className = "single-price">${price}</div>
                            <button
                                 style = {{ 
                                    cursor: "pointer", 
                                    marginRight: "10px",
                                    border: "none",
                                    outline: "none", 
                                    background: "none",
                                    fontWeight: "lighter"}} 
                                onClick = {()=>removeTotally(product_number)}>Remove</button>
                        </div>
                    ))}
                </div>
            </div>
        </Layout>
    )
}