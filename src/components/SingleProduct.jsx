import React,{useState,useEffect} from 'react';
import '../styles/cardStyles.scss'
import Button from '../confirm/Button';
import { Modal } from '../confirm/Modal';
import { Star } from './Star';

export const SingleProduct = ({path, name, price, addClick, addFav, starIcon, fav, setFav, removeFav }) => {

    const [modalStatus, setmodalStatus] = useState(false);
    const [isActive, setActive] = useState(false);
    const toggleModal = () => {
        setmodalStatus(v => !v)
        setActive(h => !h);
    }

      const makeDisable = (i) => {
          if(i === true)
          {
              return true;
        } else
        {
              return false;
        }
      }

    // const removeFromCart = (product_number) => {
    //     setFav(fav => fav.filter(item => item.product_number !== product_number))
    // }

      
    return (
        <div
            className="single-product"
        >
           
            <div className = "bg-color">
                    <div><img className = "poster-images" src = {path}/></div>
                    <div className = "details">
                        <div className = "movie-name">{name}</div>
                        <div className = "movie-desc">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellendus corrupti quod ullam.</div>
                        <div className = "cart-price">
                            <div>${price}</div>
                            <div>
                                <Button 
                                    backgroundColor='#1e1e20' 
                                    text='Add to cart' 
                                    onClick={toggleModal}
                                    disable = {makeDisable(isActive)}
                                    />
                                {(modalStatus && (
                                    <Modal
                                        header="Do you want to add this item to basket?"
                                        closeIcon = {true}
                                        backgroundColor = "#e6e6e6"
                                        text = {name}
                                        close = {toggleModal}
                                        actions = {[
                                            <Button 
                                                key = {1}
                                                backgroundColor='rgba(0,0,0,.1)' 
                                                text='Add'
                                                onClick = {() => {
                                                    addClick();
                                                    toggleModal();
                                                }}/>,
                                            <Button 
                                                key ={2}
                                                backgroundColor='rgba(0,0,0,.1)' 
                                                text='Cancel' 
                                                onClick={toggleModal}/>]} />
                                ))} 
                            </div>
                            
                        </div>
                        <div>
                                <Star
                                removeme = {removeFav}
                                addme = {addFav} 
                                src = {starIcon}/>                           
                        </div>

                    </div>
            </div>
           
            
        </div>
    )
}