import React, {useState, useEffect} from 'react';
import '../styles/cardStyles.scss'

export const Star = ({src, addme,removeme}) => {
    const [selected, setSelected] = useState(false);
    
    // const [selected, setSelected] = useState(() =>{
    //     const starData = localStorage.getItem('starStatus');
    //     return starData ? JSON.parse(starData) : false
    // })

    // useEffect(() => {
    //     localStorage.setItem('starStatus', JSON.stringify(selected))
    // },[selected])

    return(
        <img 
        src = {src} 
        onClick = {() => {
            setSelected(!selected);
            return (selected ? removeme() :addme());
        }} 
        className = {selected ? "fav-star-icon" : "star-icon"} />
    )
}