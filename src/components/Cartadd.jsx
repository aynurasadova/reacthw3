import React, { useState, useEffect } from 'react';
import '../styles/totalCardStyle.scss'

export const Cartadd = ({ cart, setCart, className }) => {

    const [display, setDisplay] = useState(false);

    const removeFromCart = (product_number, count) => {
        if (count > 1) {
            setCart(cart => cart.map(item => {
                if (item.product_number === product_number) {
                    return {
                        ...item,
                        count: item.count - 1
                    }
                }
                return item
            }))
        } 
    }

    const removeTotally = (product_number) => {
        setCart(cart => cart.filter(item => item.product_number !== product_number));
    }

    const addItem = (product_number, count) => {
        setCart(cart => cart.map((item) => {
            if(product_number === item.product_number) {
                return {
                    ...item,
                    count: item.count + 1
                }
            }
        }))
    }

    return (
        <div className={className}>
            <div className = "total-detail">
                <div>Your Basket</div>
                {cart.map(({path, name, price, product_number, count }) => (
                    <div className = "single-detail" key={product_number}>
                        <div><img style = {{width: "50px", height: "62px"}} src={path} alt="product image"/></div>
                        <div className = "product-name">"{name}"</div>
                        <div className = "single-price">${price}</div>
                        <button 
                        onClick = {() => addItem(product_number, count)}
                        >+</button>
                        <div className = "number-of-item">{count}</div>
                        <button
                            className = "remove-btn"
                            onClick={() => removeFromCart(product_number, count)}
                        >-</button>
                        <button 
                        onClick = {() => removeTotally(product_number)}
                        >X</button>
                    </div>
                ))}
            </div>
            <div className = "total-price">Total price : $
                {cart.reduce((total, { price, count }) => total + price * count, 0)}
            </div>
        </div>
    )
}