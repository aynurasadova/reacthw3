import React from 'react';
import { Header } from './Header';
import { Footer } from './Footer';
import styled from 'styled-components';

export const Layout = ({children}) => {
    return(
        <Container>
            <Header/>
                <Content>
                    {children}
                </Content>               
            <Footer/>
        </Container>
    )
}

const Container = styled.div`
    width: 100%;
    background-color: black;
    display: flex;
    flex-direction: column;
    align-items: center;
    min-height: 100vh;
`

const Content = styled.main `
    max-width: 100%;
    flex-grow: 1;
    margin: 0 auto;
`
