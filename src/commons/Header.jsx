import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

export const Header = () => {
  return(
    <HeaderStyle>
        <LogoLink to ="/"><Logo src = "/photos/marvel.png"/></LogoLink>
        <CartNavlink to = "/Cart">Cart</CartNavlink>
        <FavoritesNavlink to = "/favorites">Favorites</FavoritesNavlink>
    </HeaderStyle>
  )  
} 

const HeaderStyle = styled.header `
    width: 100%;
    height: 62px;
    border-bottom: 1px solid rgb(128,128,128);
    border-top: 1px solid rgb(128,128,128);
    display: flex;
    align-items:center;
    justify-content: center;
`

const Logo = styled.img `
    width: 140px;
    height: 60px;
`
    
const LogoLink = styled(NavLink)`
    margin-left: 37%;
    height: 100%;
`

const CartNavlink = styled(NavLink)`
    text-decoration: none;
    text-transform: uppercase;
    font-size: 18px;
    font-family: 'Sanchez';
    color:rgb(128,128,128);
    margin-left: 21%;
      &.active{
        font-weight: bold;
        color: #b8b4b4;
      }
`
    
const FavoritesNavlink = styled(NavLink)`
    text-decoration: none;
    text-transform: uppercase;
    font-size: 18px;
    margin-left: 55px;
    font-family: 'Sanchez';
    color:rgb(128,128,128);
      &.active{
        font-weight: bold;
        color: #b8b4b4;
      }
`