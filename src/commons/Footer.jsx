import React from 'react';
import styled from 'styled-components';

export const Footer = () => {
    return(
        <FooterStyle>{'\u00A9'} 2020 MARVEL</FooterStyle>
    )
}

const FooterStyle = styled.footer`
    border-bottom: 1px solid rgb(128,128,128);
    border-top: 1px solid rgb(128,128,128);
    width:100%;
    padding: 15px;
    color: white;
    text-align: center;
    font-family: 'Sanchez';
    font-size: 15px;
`