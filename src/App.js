import React, { useState, useEffect } from 'react';
import './styles/bodyStyle.scss';
import {
  SingleProduct,
  Cartadd
} from './components';
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { Layout } from './commons/Layout';
import {BrowserRouter as Router, Switch, Link, Route} from 'react-router-dom';
import { Products } from './pages/Homepage/Products';
import { Cart } from './pages/Cart';
import { Favorites } from './pages/Favorites';


function App() {
  
  const [data, setData] = useState([]);
  // const [cart, setCart] = useState(() => {
  //   const localData = localStorage.getItem('cartItem');
  //   return (localData) ? JSON.parse(localData):[];
  // });

  // useEffect(() => {
  //   localStorage.setItem('cartItem', JSON.stringify(cart));
  // },[cart]);

  // const [fav, setFav] = useState(() => {
  //   const favData = localStorage.getItem('favorites');
  //   return (favData) ? JSON.parse(favData):[];
  // });

  // useEffect(() => {
  //   localStorage.setItem('favorites', JSON.stringify(fav))
  // },[fav])

  // const [color, setColor] = useState(false);
  // const [basket, setBasket] = useState(true);
  // const [favIcon, setFavIcon] = useState(true);
  // const getData = async () => {
  //   const res = await fetch('http://localhost:3001/movies');
  //   const dt = await res.json();
  //   setData(dt);
  // }
  
  
  // const addToCart = (path, name, price, product_number, id) => {
  //   if (!isNaN(price)) {
  //     const isAvailable = cart.find((item) => product_number === item.product_number);
  //     if (isAvailable) {
  //       setCart(cart => cart.map(item => {
          
  //         if (item.product_number === product_number) {
  //           return {
  //             ...item,
  //             count: item.count + 1,
  //           }
  //         }
  //         return item
  //       }))
  //     } else {
  //       setCart(cart => [...cart, {
  //         name,
  //         price,
  //         path,
  //         id,
  //         product_number,
  //         count: 1
  //       }])
  //     }
  //   }
  // }

  // const addToFavorite = (name, price, path, product_number) => {
  //   const isAvailable = fav.find((item) => (
  //     product_number === item.product_number
  //     ))
  //     if(!isAvailable){
  //       setFav(fav => [...fav, {
  //         name,
  //         price,
  //         path,
  //         product_number,
  //     }])
  //   }
  //   else {return;}
  // }

  // useEffect(() => {
  //   getData();
  // }, [])
  

  
  // const handleFavIcons = () => {
  //   setFavIcon(prev => !prev);
  // }
  
  // const handleBasket = () => {
  //   setBasket(prev => !prev);
  // }
  
  // const removeFromCart = (product_number) => {
  //   setFav(fav => fav.filter(item => item.product_number !== product_number))
  // }

  let i = 0;
  
  return (
    <div className="App">
      <Router>
        {/* <div className = "icons">
          <div><img onClick = {handleFavIcons} className = {favIcon ? "heart-icon":"heart-icon-active"} src = "/icons/heart.svg" /></div>
          <div><img onClick = {handleBasket} className = {basket ? "basket-icon": "basket-icon-active"} src = "/icons/buy.svg" /></div>
        </div> */}
        {/* <div className = "cart-fav">
          <Cart
            className = {basket ? "notDisplayCart": "cart"}
            cart={cart}
            setCart={setCart}
          />
          <Favorites 
          className = {favIcon ?  "notDisplayFav":"fav-cart"}
          fav = {fav}
          setFav = {setFav}/>
        </div> */}

        <Switch>
          <Route exact path = "/" component = {Products} />
          <Route exact path ="/cart" component = {Cart} />
          <Route exact path = "/favorites" component = { Favorites }/>
        </Switch>
        {/* <div 
        className="products-list">     
            
                <CarouselProvider
                className = "karusel"
                naturalSlideWidth={100}
                naturalSlideHeight={45}
                totalSlides={2}
                >
                  <ButtonBack>Back</ButtonBack>
                  <ButtonNext>Next</ButtonNext>
                  <Slider 
                  className = "slider"
                  style = {{width: "100%"}}>
                    
                      {
                        data.length ? data.map(({path, name, price, product_number, id}) => (
                          <div>
                            <Slide 
                                style = {{ width: "calc(100% / 10)", border: "1px solid yellow", overflow: "ellipses"}}>
                                  {console.log(JSON.parse(id))}
                                  <SingleProduct
                                  key = { product_number }
                                  fav = {fav}
                                  setFav = {setFav}
                                  path = { path }
                                  name = { name }
                                  price = { price }
                                  starIcon = "/icons/greystar.svg"
                                  addClick = {() => addToCart(path, name, price, product_number)}
                                  addFav = {() => addToFavorite(name, price, path, product_number)}
                                  removeItem = {() => removeFromCart(product_number)}
                                  colorClass = {color ? "fav-star-icon" : "star-icon"}
                              />
                            </Slide>
                          </div>
                        
                          )): <h1>Loading</h1>
                        }
                  </Slider>
                </CarouselProvider> */}
              
            {/*             
              {
                data.map(({path, name, price, product_number}) => (
                  <SingleProduct
                    key = { product_number }
                    fav = {fav}
                    setFav = {setFav}
                    path = { path }
                    name = { name }
                    price = { price }
                    starIcon = "/icons/greystar.svg"
                    addClick = {() => addToCart(path, name, price, product_number)}
                    addFav = {() => addToFavorite(name, price, path, product_number)}
                    removeItem = {() => removeFromCart(product_number)}
                    colorClass = {color ? "fav-star-icon" : "star-icon"}
                  />
                ))
                } */}
          
          
        {/* </div> */}
      </Router>
    </div>
  );
}

export default App;
